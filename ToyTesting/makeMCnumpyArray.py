import os
import numpy as np
import ROOT 
from root_numpy import tree2array


mc16a_pathName = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16a/Nominal/mc16a.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.e6452_a875_r9364_p3663.h024.root/'
mc16d_pathName = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16d/Nominal/mc16d.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.e6452_a875_r10201_p3663.h024.root/'
mc16e_pathName = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16e/Nominal/mc16e.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.e6452_a875_r10724_p3663.h024.root/'

mc16a_files = os.listdir(mc16a_pathName)
mc16d_files = os.listdir(mc16d_pathName)
mc16e_files = os.listdir(mc16e_pathName)

mc16a_fileNames = [ mc16a_pathName+mc16a_files[i] for i in range(len(mc16a_files)) ]
mc16d_fileNames = [ mc16d_pathName+mc16d_files[i] for i in range(len(mc16d_files)) ]
mc16e_fileNames = [ mc16e_pathName+mc16e_files[i] for i in range(len(mc16e_files)) ]

lumi_mc16a = 36.2
lumi_mc16d = 44.3 
lumi_mc16e = 58.5 

branches = [ "HGamEventInfoAuxDyn.m_yy" , "HGamEventInfoAuxDyn.weight" ]
selection = "HGamEventInfoAuxDyn.isPassed"

weightHistName = "CutFlow_Sherpa2_myy_90_175_noDalitz_weighted"
treeName = "CollectionTree"

outputArray = np.empty((0,2),dtype=[('HGamEventInfoAuxDyn.m_yy', '<f4'), ('HGamEventInfoAuxDyn.weight', '<f4')])

for i,fileName in enumerate(mc16a_fileNames): 
	file = ROOT.TFile.Open(fileName)
	tree = file.Get(treeName) 
	array = tree2array(tree, branches=branches, selection=selection) 
	weightHist = file.Get(weightHistName)
	extraWeight = weightHist.GetBinContent(1) / weightHist.GetBinContent(2) * weightHist.GetBinContent(3)
	array['HGamEventInfoAuxDyn.weight'] = array['HGamEventInfoAuxDyn.weight'] * lumi_mc16a / ( extraWeight * len(mc16a_files) )
	outputArray = np.append( outputArray , array )
	file.Close()
	
for i,fileName in enumerate(mc16d_fileNames): 
	file = ROOT.TFile.Open(fileName)
	tree = file.Get(treeName) 
	array = tree2array(tree, branches=branches, selection=selection) 
	weightHist = file.Get(weightHistName)
	extraWeight = weightHist.GetBinContent(1) / weightHist.GetBinContent(2) * weightHist.GetBinContent(3)
	array['HGamEventInfoAuxDyn.weight'] = array['HGamEventInfoAuxDyn.weight'] * lumi_mc16d / ( extraWeight * len(mc16d_files) )
	outputArray = np.append( outputArray , array )
	file.Close()
	
for i,fileName in enumerate(mc16e_fileNames): 
	file = ROOT.TFile.Open(fileName)
	tree = file.Get(treeName) 
	array = tree2array(tree, branches=branches, selection=selection) 
	weightHist = file.Get(weightHistName)
	extraWeight = weightHist.GetBinContent(1) / weightHist.GetBinContent(2) * weightHist.GetBinContent(3)
	array['HGamEventInfoAuxDyn.weight'] = array['HGamEventInfoAuxDyn.weight'] * lumi_mc16e / ( extraWeight * len(mc16e_files) )
	outputArray = np.append( outputArray , array ) 
	file.Close()
	
np.save('slimmed_MCnumpyArray.npy', outputArray) 
	
