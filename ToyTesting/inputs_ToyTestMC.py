# The name of the input numpy file containing the many Myy values and the event weights 
inFileName = "slimmed_MCnumpyArray.npy" 

# The bounds on Myy to apply, in GeV
xMin = 105			
xMax = 160

# The number of bins in Myy 
nBins = 55

# The different levels of statistics used for making toys 
statistics = [ 500 , 1000 , 5000 , 10000 , 100000 , 1000000 ] 

# The hyper-parameter ranges to be used, if not using the outputs of the hyper-par optimization 
Gibbs_l0_bounds = [8,12]
Gibbs_l_slope_bounds = [0.01,0.5]

# The number of bins to combine, if rebinning input histogram (optional, but can make the GPR fit run faster if the input histogram has very fine binning) 
nRebin = 1

# How many toys to generate? Recommended to make this a small number and run many instances in parallel on condor
nToys = 1

# The Myy values over which to perform the S+B fit 
masses = [ 125. ]
#masses = [ 121. , 122. , 123. , 124. , 125. , 126. , 127. , 128. , 129. ]

# Which analytical functions to test by fitting to the toy 
myPDFs = [ "PowerLaw" , "Exponential" , "ExpPoly2" , "ExpPoly3" , "Bern3" , "Bern4" , "Bern5" ]

# The percentage of injected signal-like events to inject (optional) 					
sigFrac = 0.0

# The name of the output file containing the 2D histograms of the optimal hyper params  
outFileName = "GPRToyTestsMC.root" 
