# The name of the input file containing the input MC template 
inFileName = "/afs/cern.ch/work/r/rahynema/public/ttHyy/GaSBaG/inputTemplates/bkg_templates.root" 

# The name of the histogram containing the unsmoothed template
templateHistoNames = [ 	"Template_ggH_0J_Cen" ,
						"Template_ggH_0J_Fwd" , 
						"Template_ggH_1J_LOW" , 
						"Template_ggH_1J_MED" , 
						"Template_ggH_1J_HIGH" ,
						"Template_ggH_1J_BSM" , 
						"Template_ggH_2J_LOW" , 
						"Template_ggH_2J_MED" , 
						"Template_ggH_2J_HIGH" ,
						"Template_VBF_HjjLOW_loose" , 
						"Template_VBF_HjjLOW_tight" , 
						"Template_VBF_HjjHIGH_loose" , 
						"Template_VBF_HjjHIGH_tight" , 
						"Template_VHhad_loose" , 
						"Template_VHhad_tight" , 
						"Template_qqH_BSM" , 
						"Template_VHMET_LOW" , 
						"Template_VHMET_HIGH" , 
						"Template_VHlep_LOW" , 
						"Template_VHlep_HIGH" , 
						"Template_VHdilep" ]

# A list of the names of the categories for which templates have been provided (optional) 
#   If empty, will default to the above list of template names 
categoryNames = [ 	"ggH 0J Central" , 
					"ggH 0J Forward" , 
					"ggH 1J Low" ,
					"ggH 1J Medium" , 
					"ggH 1J High" , 
					"ggH 1J BSM" , 
					"ggH 2J Low" ,
					"ggH 2J Medium" , 
					"ggH 2J High" , 
					"VBF H_{jj}^{Low} Loose" , 
					"VBF H_{jj}^{Low} Tight" , 
					"VBF H_{jj}^{High} Loose" , 
					"VBF H_{jj}^{High} Tight" , 
					"VHhad Loose" , 
					"VHhad Tight" , 
					"qqH BSM" , 
					"VHMET Low" , 
					"VHMET High" , 
					"VH lep Low" , 
					"VH lep High" , 
					"VH dilep" ]
				
# The name of the file containing the optimized Gibbs hyper-parameter ranges for each category 
hyperParFileName = "../GPR_HyperParameter_Optimization.root"

# The hyper-parameter ranges to be used, if not using the outputs of the hyper-par optimization 
Gibbs_l0_bounds = [8,12]
Gibbs_l_slope_bounds = [0.01,0.5]

# The number of bins to combine, if rebinning input histogram (optional, but can make the GPR fit run faster if the input histogram has very fine binning) 
nRebin = 1

# How many toys to generate? Recommended to make this a small number and run many instances in parallel on condor
nToys = 1

# The Myy values over which to perform the S+B fit 
masses = [ 125. ]
#masses = [ 121. , 122. , 123. , 124. , 125. , 126. , 127. , 128. , 129. ]

# Which analytical functions to test by fitting to the toy 
myPDFs = [ "PowerLaw" , "Exponential" , "ExpPoly2" , "ExpPoly3" , "Bern3" , "Bern4" , "Bern5" ]

# The percentage of injected signal-like events to inject (optional) 					
sigFrac = 0.0

# The name of the output file containing the 2D histograms of the optimal hyper params  
outFileName = "GPRToyTests.root" 
