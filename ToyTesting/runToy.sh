#!/bin/sh
date
echo "Running on the host: `/bin/hostname`"

#################### ANALYSIS SHELL SCRIPT ########################

echo "Current directory: `pwd`"

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "lcgenv -p LCG_95apython3 x86_64-centos7-gcc7-opt Python"
lsetup "lcgenv -p LCG_95apython3 x86_64-centos7-gcc7-opt ROOT"
lsetup "lcgenv -p LCG_95apython3 x86_64-centos7-gcc7-opt numpy"
lsetup "lcgenv -p LCG_95apython3 x86_64-centos7-gcc7-opt scikitlearn"
lsetup "lcgenv -p LCG_95apython3 x86_64-centos7-gcc7-opt scipy"

export PYTHONHOME=/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/Python/3.6.5/x86_64-centos7-gcc7-opt
export PYTHONPATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/numpy/1.14.2/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/Python/3.6.5/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/setuptools/36.0.1/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/scipy/1.1.0/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/numpy/1.14.2/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/Python/3.6.5/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/setuptools/36.0.1/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/scipy/1.1.0/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/scikitlearn/0.19.2/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/Python/3.6.5/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/setuptools/36.0.1/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/numpy/1.14.2/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/ROOT/6.16.00/x86_64-centos7-gcc7-opt/lib:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/xrootd/4.8.4/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/setuptools/36.0.1/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/numpy/1.14.2/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/Python/3.6.5/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/releases/LCG_95apython3/Python/3.6.5/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:

echo 'Starting...'

cp -r /afs/cern.ch/work/r/rahynema/public/ttHyy/GaSBaG/inc .
cp /afs/cern.ch/work/r/rahynema/public/ttHyy/GaSBaG/ToyTesting/inputs_ToyTest.py .
cp /afs/cern.ch/work/r/rahynema/public/ttHyy/GaSBaG/ToyTesting/toyTest.py .
python toyTest.py 

echo 'Ending...'


###############################################################

echo "Done."
