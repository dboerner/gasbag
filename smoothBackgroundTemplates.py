# Script written by Rachel Hyneman, 2019 
# Contact: rachel.hyneman@cern.ch 

import numpy as np
from scipy.optimize import curve_fit
import ROOT 
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel 
import sys 
sys.path.append('inc/')
from FallExpKernel import * 
from LinearErrorKernel import * 


########### User Defined Options ##########

# These are all pulled from the input file below: 

from inputs_SmoothBackgroundTemplates import * 

###########################################

### Basic Exponential for GPR Prior 

def mySimpleExponential(x, a, b):
	return a*np.exp( b * x ) 		
	
###########################################

### A quick function to create histograms from numpy arrays 

def makeHistoFromArray( inputArray , outputHisto , inputError = np.array('i') ): 
	nBins = outputHisto.GetNbinsX() 
	if( len(inputArray) != nBins ): 
		print("WARNING in makeHistoFromArray")
		print("Number of bins in histogram does not match length of input array!")
		print("Returning empty histogram.") 
		return 
	for iBin in range(nBins): 
		outputHisto.SetBinContent( iBin+1 , inputArray[iBin] ) 
		if( inputError.shape ): outputHisto.SetBinError( iBin+1 , inputError[iBin] ) 

###########################################

### A quick function to create numpy arrays from histograms

def makeArrayFromHisto( inputHisto ): 
	nBins = inputHisto.GetNbinsX()
	outputArray = np.zeros( nBins , 'd' )
	for iBin in range(nBins): 
		outputArray[iBin] = inputHisto.GetBinContent( iBin+1 ) 
	return outputArray
	
###########################################
########### The Important Stuff ###########

nCategories = len(templateHistoNames) 

if ( not not inDataSBFileName ): 
	if ( len(sideBandRefNames) != nCategories ): 
		print( 	"WARNING - Input data sidebands file has been provided, but there are not enough SB histograms passed. Will NOT include side bands!" )
		inDataSBFileName = "" 
		
if ( len(categoryNames) != nCategories ): 
	print( 	"WARNING - The number of category names passed is not the same as the number of background templates passed! Will ignore the given category names." )
	categoryNames = templateHistoNames 

if( not not hyperParFileName ): hyperParFile = ROOT.TFile.Open(hyperParFileName)

outTemplateFile = ROOT.TFile(outTemplateFileName,"RECREATE") 
ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasStyle.C") 
ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasUtils.C") 
ROOT.SetAtlasStyle()

for iCat in range(nCategories): 

	### Setup all inputs before applying GPR 
	
	inHistName = templateHistoNames[ iCat ]
	if( len(categoryNames) ): catName = categoryNames[ iCat ] 
	else: catName = inHistName
	
	print( "Applying GPR Smoothing to Category: ", catName )

	# Get the input template histogram 

	inFile = ROOT.TFile.Open(inFileName,"READ")
	inHisto = inFile.Get(inHistName)
	
	if( not inHisto ): 
		print( "ERROR - Template '"+inHistName+"' not found!! Check template name and file contents." )
		continue 
		
	# Get some information from the input template  

	inHisto.Rebin( nRebin )

	nEvts = inHisto.GetEntries() 

	nBins = inHisto.GetNbinsX()
	xMin = inHisto.GetBinLowEdge(1)
	xMax = inHisto.GetBinLowEdge(nBins+1)

	x_Myy = np.array( [ xMin+1.0*i*(xMax-xMin)/nBins for i in range(nBins) ] )

	# Get the data sidebands from the file (if specified) 

	if ( not not inDataSBFileName ): 
		inDataSBFile = ROOT.TFile(inDataSBFileName) 
		sideBandRefName = sideBandRefNames[iCat]
		sideBandRefHisto = inDataSBFile.Get( sideBandRefName )
		if ( not sideBandRefHisto ): 
			print( "ERROR - Sidebands histogram '"+sideBandRefName+"' not found!! Check histogram name and file contents." )
			continue 	
	else: # make a dummy histo if not looking at data sidebands 
		sideBandRefHisto = ROOT.TH1F("sideBandRefHisto","sideBandRefHisto",nBins,xMin,xMax)
		sideBandRefName = ""	

	# Make a numpy array from the input template and sideband histograms 

	myyHistArray = makeArrayFromHisto( inHisto )
	sideBandRefArray = makeArrayFromHisto( sideBandRefHisto )

	# Make a numpy array of the error bars from the input template and sideband histograms 

	myyErrs = [] 
	sbRefErrs = [] 
	for ibin in range( inHisto.GetNbinsX() ): 
		myyErrs.append( inHisto.GetBinError( ibin+1 ) )
		sbRefErrs.append( sideBandRefHisto.GetBinError( ibin+1 ) )
	myyErrs = np.array( myyErrs ) 

	### Define the GPR Prior (should be reasonably similar to the input template shape) 

	# Assume Linear Prior

	#baseline_slope_est = ( np.max(myyHistArray) - np.min(myyHistArray) ) / (x_Myy[0]-x_Myy[-1])
	#myBaseLine = np.array([ np.max(myyHistArray) + baseline_slope_est*(x-x_Myy[0]) for x in x_Myy ])

	# Assume Exponential Prior (Recommended) 

	optimal_pars, par_cov = curve_fit(mySimpleExponential, x_Myy, myyHistArray, p0=(myyHistArray[0],-0.02))
	myBaseLine = np.array(mySimpleExponential(x_Myy,optimal_pars[0],optimal_pars[1]))

	### Get estimates of different hyper-parameter bounds from the input template 

	# Set bounds on the scaling of the GPR template (basically a maximum y-range) 

	const_low = 10.0**-5
	const_hi = nEvts * 10.0
	
	# Get the kernel hyper-parameters from the input file 
	
	if( not not hyperParFileName ): 
		hyperParTree = hyperParFile.Get( "HyperParArea_"+inHistName )
		hyperParTree.GetEntry(0)
		Gibbs_l0_bounds[0] =  hyperParTree.GetLeaf("length_scale_min").GetValue() 
		Gibbs_l0_bounds[1] =  hyperParTree.GetLeaf("length_scale_max").GetValue()
		Gibbs_l_slope_bounds[0] =  hyperParTree.GetLeaf("length_scale_slope_min").GetValue()
		Gibbs_l_slope_bounds[1] =  hyperParTree.GetLeaf("length_scale_slope_max").GetValue()
	
	Gibbs_l0 = Gibbs_l0_bounds[0] + 0.9*( Gibbs_l0_bounds[1] - Gibbs_l0_bounds[0] )
	Gibbs_l_slope = Gibbs_l_slope_bounds[0] + 0.9*( Gibbs_l_slope_bounds[1] - Gibbs_l_slope_bounds[0] )

	# Set bounds on the magnitude of the GPR error bars 

	errConst0 = np.max(myyErrs[0])
	errConst_low = 0.90*np.max(myyErrs[0])
	errConst_hi = 1.10*np.max(myyErrs[0])	

	# Estimate the (decreasing) slope of the magnitude of the GPR error bars 

	slope_est0 = -1.0*(myyErrs[0]-myyErrs[-1])/(x_Myy[0]-x_Myy[-1])
	if( slope_est0 < 10**-5 ): slope_est0 = 10**-4
	slope_est_low = 0.5*slope_est0
	slope_est_hi = 1.05*slope_est0
	
	### Define the GPR Kernel 

	kernel = ( ConstantKernel(constant_value=1.0, constant_value_bounds=(const_low,const_hi)) * 
				Gibbs(l0=Gibbs_l0,l0_bounds=(Gibbs_l0_bounds[0],Gibbs_l0_bounds[1]),l_slope=Gibbs_l_slope,l_slope_bounds=(Gibbs_l_slope_bounds[0],Gibbs_l_slope_bounds[1])) +
			   ConstantKernel(constant_value=1.0, constant_value_bounds=(1.0,1.0)) * 
				LinearNoiseKernel(noise_level=errConst0,noise_level_bounds=(errConst_low,errConst_hi),b=slope_est0,b_bounds=(slope_est_low,slope_est_hi)) )

	### Fit the GPR Kernel to the input template 
	
	gpr = GaussianProcessRegressor( kernel=kernel , n_restarts_optimizer = 3 ) 
	gpr.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )
	
	# Check to see if GPR Prediction is effectively the same as the exponential prior 
	# If so, re-do the GPR fitting, but with a flat prior instead of exponential prior 
	
	gprPrediction, gprCov = gpr.predict( x_Myy.reshape(len(x_Myy),1) , return_cov=True )
	
	if( np.sqrt( (gprPrediction*gprPrediction).sum() )/ myyHistArray.sum() < 0.0001 ): 
		myBaseLine = np.zeros( len(myyHistArray) ) 
		gpr = GaussianProcessRegressor( kernel=kernel , n_restarts_optimizer = 3 , normalize_y=True ) 
		gpr.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )	
		gprPrediction, gprCov = gpr.predict( x_Myy.reshape(len(x_Myy),1) , return_cov=True )

	# Use the GPR template's errors 
	gprError = np.copy( np.diagonal(gprCov) )
	gprPrediction = gprPrediction + myBaseLine
	gprPredictionHisto = ROOT.TH1F( "gprPredictionHisto" , "gprPredictionHisto" , nBins , xMin , xMax ) 
	makeHistoFromArray( gprPrediction , gprPredictionHisto , gprError )

	# Use Poisson stat errors for GPR template (Recommended) 
# 	gprPrediction = gpr.predict( x_Myy.reshape(len(x_Myy),1) , return_std=False )
# 	gprPrediction = gprPrediction + myBaseLine
# 	gprError = [] 
# 	for i in range(len(gprPrediction)): 
# 		gprError.append( np.sqrt( gprPrediction[i] ) ) 
# 	gprError = np.array(gprError)

	### Calculate Systematic Errors by Varying the Length Scale 
	
	if( calculateSystematics ):
	
		params = gpr.kernel_.get_params()
	
		nominalNoiseLevel = params['k2__k2__noise_level']
		nominalNoiseSlope = params['k2__k2__b']
		nominalConst = params['k1__k1__constant_value']
		nominalGibbsSlope = params['k1__k2__l_slope']
		nominalLengthScale = params['k1__k2__l0']
		lengthScale_systUp = 1.2*nominalLengthScale
		lengthScale_systDown = 0.8*nominalLengthScale
	
		# Fix all parameters, except Gibbs length scale normalization, to nominal fitted kernel params
# 		kernel_systUp = ( ConstantKernel(constant_value=1.0, constant_value_bounds=(const_low,const_hi)) * 
# 					Gibbs(l0=lengthScale_systUp,l0_bounds=(lengthScale_systUp,lengthScale_systUp),l_slope=nominalGibbsSlope,l_slope_bounds=(nominalGibbsSlope,nominalGibbsSlope)) +
# 				   ConstantKernel(constant_value=1.0, constant_value_bounds=(1.0,1.0)) * 
# 					LinearNoiseKernel(noise_level=nominalNoiseLevel,noise_level_bounds=(nominalNoiseLevel,nominalNoiseLevel),b=nominalNoiseSlope,b_bounds=(nominalNoiseSlope,nominalNoiseSlope)) )
	
# 		kernel_systDown = ( ConstantKernel(constant_value=1.0, constant_value_bounds=(const_low,const_hi)) * 
# 					Gibbs(l0=lengthScale_systDown,l0_bounds=(lengthScale_systDown,lengthScale_systDown),l_slope=nominalGibbsSlope,l_slope_bounds=(nominalGibbsSlope,nominalGibbsSlope)) +
# 				   ConstantKernel(constant_value=1.0, constant_value_bounds=(1.0,1.0)) * 
# 					LinearNoiseKernel(noise_level=nominalNoiseLevel,noise_level_bounds=(nominalNoiseLevel,nominalNoiseLevel),b=nominalNoiseSlope,b_bounds=(nominalNoiseSlope,nominalNoiseSlope)) )
		
		# Fix only Gibbs length scale 
		kernel_systUp = ( ConstantKernel(constant_value=1.0, constant_value_bounds=(const_low,const_hi)) * 
				Gibbs(l0=lengthScale_systUp,l0_bounds=(lengthScale_systUp,lengthScale_systUp),l_slope=Gibbs_l_slope,l_slope_bounds=(Gibbs_l_slope_bounds[0],Gibbs_l_slope_bounds[1])) +
			   ConstantKernel(constant_value=1.0, constant_value_bounds=(1.0,1.0)) * 
				LinearNoiseKernel(noise_level=errConst0,noise_level_bounds=(errConst_low,errConst_hi),b=slope_est0,b_bounds=(slope_est_low,slope_est_hi)) )

		kernel_systDown = ( ConstantKernel(constant_value=1.0, constant_value_bounds=(const_low,const_hi)) * 
				Gibbs(l0=lengthScale_systDown,l0_bounds=(lengthScale_systDown,lengthScale_systDown),l_slope=Gibbs_l_slope,l_slope_bounds=(Gibbs_l_slope_bounds[0],Gibbs_l_slope_bounds[1])) +
			   ConstantKernel(constant_value=1.0, constant_value_bounds=(1.0,1.0)) * 
				LinearNoiseKernel(noise_level=errConst0,noise_level_bounds=(errConst_low,errConst_hi),b=slope_est0,b_bounds=(slope_est_low,slope_est_hi)) )

	
		gpr_systUp = GaussianProcessRegressor( kernel=kernel_systUp , n_restarts_optimizer = 1 )
		gpr_systDown = GaussianProcessRegressor( kernel=kernel_systDown , n_restarts_optimizer = 1 )
		
		gpr_systUp.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )
		gpr_systDown.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )
		
		gprPrediction_systUp = gpr_systUp.predict( x_Myy.reshape(len(x_Myy),1) )
		gprPrediction_systUp = gprPrediction_systUp + myBaseLine
		
		gprPrediction_systDown = gpr_systDown.predict( x_Myy.reshape(len(x_Myy),1) )
		gprPrediction_systDown = gprPrediction_systDown + myBaseLine
		
		gprHiErrors = [] 
		gprLowErrors = [] 
		gprSystErrors = []
		for i in range(len(gprError)): 
			gprHiErrors.append( np.max([gprPrediction_systUp[i],gprPrediction_systDown[i]]) - gprPrediction[i] )
			gprLowErrors.append( gprPrediction[i] - np.min([gprPrediction_systUp[i],gprPrediction_systDown[i]]) )
			if( gprHiErrors[i] < 0.0 ): gprHiErrors[i] = 0.0 
			if( gprLowErrors[i] < 0.0 ): gprLowErrors[i] = 0.0 
			gprSystErrors.append( np.max([gprHiErrors[i],gprLowErrors[i]]) )
			gprError[i] += gprSystErrors[i]
		gprHiErrors = np.array(gprHiErrors)
		gprLowErrors = np.array(gprLowErrors)
		gprSystErrors = np.array(gprSystErrors)
		
		# Histogram with the systematic errors only (for plotting) 
		templateName = "GPR_Smoothed_"+inHistName+"_systErr" 
		bkgTemplateSystErr = ROOT.TH1F( templateName , templateName , nBins , xMin , xMax ) 
		makeHistoFromArray( gprPrediction, bkgTemplateSystErr , gprSystErrors )
		
		# Below are the histograms of the GPR fits with systematically varied length scale 
		templateName = "GPR_Smoothed_"+inHistName+"_systUp" 
		bkgTemplateSystUp = ROOT.TH1F( templateName , templateName , nBins , xMin , xMax ) 
		makeHistoFromArray( gprPrediction_systUp, bkgTemplateSystUp )
		
		templateName = "GPR_Smoothed_"+inHistName+"_systDown" 
		bkgTemplateSystDown = ROOT.TH1F( templateName , templateName , nBins , xMin , xMax ) 
		makeHistoFromArray( gprPrediction_systDown, bkgTemplateSystDown )

	### Make Output Bkg Template 

	templateName = "GPR_Smoothed_"+inHistName
	bkgTemplate = ROOT.TH1F( templateName , templateName , nBins , xMin , xMax ) 
	makeHistoFromArray( gprPrediction, bkgTemplate , gprError )
	bkgTemplate.SetName(templateName)
	outTemplateFile.cd() 
	bkgTemplate.Write() 
	
	### If specified, save the original background template and the data sidebands 
	
	if( saveInputHistograms ): 
		inHisto.Write() 
		if ( not not inDataSBFileName ): sideBandRefHisto.Write() 

	### Lost of Details to Make Nice Plots!  

	canvasName = "canvas_"+inHistName
	canvas = ROOT.TCanvas(canvasName,canvasName,600,600) 
	canvas.cd() 
	upperPad = ROOT.TPad("upperPad", "upperPad", 0.0, 0.3, 1.0, 1.0)
	lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0.0, 0.0, 1.0, 0.3)
	upperPad.Draw() 
	upperPad.SetBottomMargin(0.02)
	lowerPad.Draw() 
	lowerPad.SetTopMargin(0.02) ;
	lowerPad.SetBottomMargin(0.3) ;

	# The upper pad 

	canvas.cd() 
	upperPad.cd() 

	yMax = 1.35*inHisto.GetMaximum()
	leg = ROOT.TLegend(0.65,0.65,0.89,0.89)
	leg.SetBorderSize(0)
	leg.SetFillStyle(-1)

	if ( not not sideBandRefName ): 
		sideBandRefHisto.SetMarkerColor(1)
		sideBandRefHisto.SetMarkerStyle(8)
		sideBandRefHisto.SetLineColor(1)
		sideBandRefHisto.SetLineWidth(1) 
		sideBandRefHisto.SetMinimum(0)
		sideBandRefHisto.SetMaximum(yMax)
		sideBandRefHisto.GetYaxis().SetTitle("Events / Bin")
		sideBandRefHisto.GetYaxis().SetTitleSize(0.055)
		sideBandRefHisto.GetXaxis().SetLabelSize(0.0)
		sideBandRefHisto.Draw("ep")
		leg.AddEntry(sideBandRefHisto,"Data Sidebands","p")


	bkgTemplate.SetLineColor(601)
	bkgTemplate.SetLineWidth(2)
	bkgTemplate.SetMarkerColor(601)
	bkgTemplate.SetMarkerSize(0)
	bkgTemplate.SetFillColorAlpha(601,0.3)
	bkgTemplate.SetFillStyle(3003)
	bkgTemplate.SetMinimum(0)
	bkgTemplate.SetMaximum(yMax)
	bkgTemplate.GetXaxis().SetTitleSize(0.0)
	bkgTemplate.GetXaxis().SetLabelSize(0.0)
	bkgTemplate.GetYaxis().SetTitle("Events / Bin")
	bkgTemplate.GetYaxis().SetTitleSize(0.055)
	bkgTemplate.Draw("e4 same")
	leg.AddEntry(bkgTemplate,"GPR Smoothed Template")
	
	if( calculateSystematics ): 
		bkgTemplateSystErr.SetLineColor(870)
		bkgTemplateSystErr.SetLineStyle(2)
		bkgTemplateSystErr.SetLineWidth(2)
		bkgTemplateSystErr.SetMarkerColor(870)
		bkgTemplateSystErr.SetMarkerSize(0)
		bkgTemplateSystErr.SetFillColorAlpha(851,0.3)
		bkgTemplateSystErr.SetFillStyle(3001)
		bkgTemplateSystErr.Draw("e4 same")
		leg.AddEntry(bkgTemplateSystErr,"GPR Systematic Errors")
		
		bkgTemplateSystUp.SetLineColor(870)
		bkgTemplateSystUp.SetLineStyle(2)
		bkgTemplateSystUp.SetLineWidth(1)
		bkgTemplateSystUp.SetMarkerSize(0)
		bkgTemplateSystUp.Draw("same")
		
		bkgTemplateSystDown.SetLineColor(870)
		bkgTemplateSystDown.SetLineStyle(2)
		bkgTemplateSystDown.SetLineWidth(1)
		bkgTemplateSystDown.SetMarkerSize(0)
		bkgTemplateSystDown.Draw("same")
	
	inHisto.SetLineColor(633)
	inHisto.SetLineWidth(2)
	inHisto.SetMarkerSize(0)
	inHisto.SetMinimum(0)
	inHisto.SetMaximum(yMax)
	inHisto.GetYaxis().SetTitle("Events / Bin")
	inHisto.GetYaxis().SetTitleSize(0.055)
	inHisto.GetXaxis().SetLabelSize(0.0)
	inHisto.Draw("ehist same")
	leg.AddEntry(inHisto,"Original Template")	
	
	bkgTemplateCentral = bkgTemplate.Clone("bkgTemplateCentral")
	bkgTemplateCentral.SetLineColor(601)
	bkgTemplateCentral.SetLineWidth(2)
	bkgTemplateCentral.SetMarkerColor(601)
	bkgTemplateCentral.SetMarkerSize(0)
	bkgTemplateCentral.SetFillStyle(0)
	bkgTemplateCentral.Draw("hist same")

	leg.Draw() 	

	ROOT.ATLAS_LABEL(0.23,0.85)
	ROOT.myText(0.34,0.85,1,"Internal")
	ROOT.myText(0.23,0.80,1,"GPR Smoothed Template",0.04)
	ROOT.myText(0.23,0.76,1,catName,0.04)
	
	canvas.Update()

	# The lower pad 
	#    NOTE: If data sidebands are provided, will plot the difference of the original and 
	#    smoothed templates in reference to the data sidebands. Otherwise, will plot only the
	#    difference of the GPR smoothed template to the original. 

	canvas.cd() 
	lowerPad.cd()

	refLine = ROOT.TH1F("refLine","refLine",1,xMin,xMax)
	refLine.SetBinContent(1,0)
	refLine.SetLineColor(1)
	refLine.SetLineWidth(1)
	refLine.SetLineStyle(7)
	refLine.SetMinimum(-0.29)
	refLine.SetMaximum(0.29)
	refLine.GetXaxis().SetTitle("M_{#gamma#gamma} [GeV]")
	refLine.GetXaxis().SetTitleSize(0.13)
	refLine.GetXaxis().SetTitleOffset(1.0)
	refLine.GetXaxis().SetLabelSize(0.09)
	refLine.GetYaxis().SetTitleSize(0.11)
	refLine.GetYaxis().SetTitleOffset(0.55)
	refLine.GetYaxis().SetLabelSize(0.09)
	refLine.Draw()


	if( not not sideBandRefName ): # Plot scaled residuals of both templates w.r.t. data sidebands 
		inHistoResidual = inHisto.Clone("inHistoResidual")
		inHistoResidual.Add(sideBandRefHisto,-1)
		inHistoResidual.Divide(sideBandRefHisto)
		inHistoResidual.SetLineColor(633)
		inHistoResidual.Draw("ehist same")
		bkgTemplateResidual = bkgTemplate.Clone("bkgTemplateResidual")
		bkgTemplateResidual.Add(sideBandRefHisto,-1)
		bkgTemplateResidual.Divide(sideBandRefHisto)
		bkgTemplateResidual.SetLineColor(601)
		bkgTemplateResidual.Draw("ehist same")
		refLine.GetYaxis().SetTitle("#frac{Template - Data}{Data}")

	else: # Plot scaled residuals of GPR template w.r.t. original template
		if( calculateSystematics ): 
			bkgTemplateResSyst = bkgTemplateSystErr.Clone("bkgTemplateResSyst")
			bkgTemplateResSyst.Add(inHisto,-1)
			bkgTemplateResSyst.Divide(inHisto)
			bkgTemplateResSyst.SetLineWidth(0)
			bkgTemplateResSyst.SetLineColor(601)
			bkgTemplateResSyst.Draw("e3 same")
			
		bkgTemplateResidual = bkgTemplate.Clone("bkgTemplateResidual")
		bkgTemplateResidual.Add(inHisto,-1)
		bkgTemplateResidual.Divide(inHisto)
		bkgTemplateResidual.SetLineColor(601)
		bkgTemplateResidual.Draw("ehist same")
		
		refLine.GetYaxis().SetTitle("#frac{GPR - Original}{Original}")
	
	refLine.Draw("same")

	canvas.Update()
	canvas.Print("GPR_Smoothed_Plot_"+inHistName+".eps")
	canvas.Close() 
	
	refLine.Delete() 
	
### Close the output file 

inFile.Close() 
outTemplateFile.Close() 

print( "All done!" )