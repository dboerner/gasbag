# The name of the input file containing the input MC template 
inFileName = "inTemplates/bkg_templates.root " 

# The name of the histogram containing the unsmoothed template
#templateHistoNames = [ "Template_VHdilep" ]
templateHistoNames = [ 	"Template_ggH_0J_Cen" ,
						"Template_ggH_0J_Fwd" , 
						"Template_ggH_1J_LOW" , 
						"Template_ggH_1J_MED" , 
						"Template_ggH_1J_HIGH" ,
						"Template_ggH_1J_BSM" , 
						"Template_ggH_2J_LOW" , 
						"Template_ggH_2J_MED" , 
						"Template_ggH_2J_HIGH" ,
						"Template_ggH_2J_BSM" ,
						"Template_VBF_HjjLOW_loose" , 
						"Template_VBF_HjjLOW_tight" , 
						"Template_VBF_HjjHIGH_loose" , 
						"Template_VBF_HjjHIGH_tight" , 
						"Template_VHhad_loose" , 
						"Template_VHhad_tight" , 
						"Template_qqH_BSM" , 
						"Template_VHMET_LOW" , 
						"Template_VHMET_HIGH" , 
						"Template_VHlep_LOW" , 
						"Template_VHlep_HIGH" , 
						"Template_VHdilep" ]

# A list of the names of the categories for which templates have been provided (optional) 
#   If empty, will default to the above list of template names 
#categoryNames = ["VH dilep"]
categoryNames = [ 	"ggH 0J Central" , 
					"ggH 0J Forward" , 
					"ggH 1J Low" ,
					"ggH 1J Medium" , 
					"ggH 1J High" , 
					"ggH 1J BSM" , 
					"ggH 2J Low" ,
					"ggH 2J Medium" , 
					"ggH 2J High" ,
					"ggH 2J BSM" , 
					"VBF H_{jj}^{Low} Loose" , 
					"VBF H_{jj}^{Low} Tight" , 
					"VBF H_{jj}^{High} Loose" , 
					"VBF H_{jj}^{High} Tight" , 
					"VHhad Loose" , 
					"VHhad Tight" , 
					"qqH BSM" , 
					"VHMET Low" , 
					"VHMET High" , 
					"VH lep Low" , 
					"VH lep High" , 
					"VH dilep" ]


# The number of bins to combine, if rebinning input histogram (optional, but can make the GPR fit run faster if the input histogram has very fine binning) 
nRebin = 1

# The possible parameters for the Gibbs kernel that will be tested 
length_scales = [ 3 , 5 , 7 , 8 , 10 , 12 , 15 , 20 ]
length_scale_slopes = [ 0.01 , 0.05 , 0.1 , 0.5 ]

# The name of the ROOT file containing the fitted signal shapes (from the signal parameterization code) 
# The name of the workspace and the names of the fitted width parameters (sigmaCBNom_mc16_h21_v0.3_m125000_c# by default) must also be provided 
# If left blank, then the provided signal widths in the option below will be used instead 
inSignalFileName = "res_SM_DoubleCB_workspace.root"
inSignalWorkspaceName = "signalWS"
signalFileSigmaNames = [ 	"sigmaCB_SM_m125000_c0" , 
							"sigmaCB_SM_m125000_c1" ,
							"sigmaCB_SM_m125000_c2" ,
							"sigmaCB_SM_m125000_c3" ,
							"sigmaCB_SM_m125000_c4" ,
							"sigmaCB_SM_m125000_c5" ,
							"sigmaCB_SM_m125000_c6" ,
							"sigmaCB_SM_m125000_c7" ,
							"sigmaCB_SM_m125000_c8" ,
							"sigmaCB_SM_m125000_c9" ,
							"sigmaCB_SM_m125000_c10" ,
							"sigmaCB_SM_m125000_c11" ,
							"sigmaCB_SM_m125000_c12" ,
							"sigmaCB_SM_m125000_c13" ,
							"sigmaCB_SM_m125000_c14" ,
							"sigmaCB_SM_m125000_c15" ,
							"sigmaCB_SM_m125000_c16" ,
							"sigmaCB_SM_m125000_c17" ,
							"sigmaCB_SM_m125000_c19" ,
							"sigmaCB_SM_m125000_c20" ,
							"sigmaCB_SM_m125000_c21" , 
							"sigmaCB_SM_m125000_c23" ]

# The fitted signal widths (fitted sigma_CB) for each category, from the signal parameterization code 
# Can also provide a single value to be used for all categories 
#signalWidths = [ 1.69 ] 
signalWidths = [ 	1.61 , 
					2.03 , 
					1.84 , 
					1.74 , 
					1.55 , 
					1.39 , 
					1.85 , 
					1.73 , 
					1.56 , 
					1.77 , 
					1.57 , 
					1.72 , 
					1.56 , 
					1.64 , 
					1.49 , 
					1.34 , 
					1.76 , 
					1.49 , 
					1.75 , 
					1.51 , 
					1.69 ]

# The expected signal position in Myy 
signalPos = 125.0

# The percentage of injected signal-like events to inject (1% recommended) 					
sigFrac = 0.01

# The name of the output file containing the 2D histograms of the optimal hyper params  
outFileName = "GPR_HyperParameter_Optimization.root" 
