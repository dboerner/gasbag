import numpy as np
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from scipy.optimize import curve_fit
import ROOT 
#from root_numpy import hist2array, fill_hist
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import ConstantKernel 
import sys 
sys.path.append('inc/')
from FallExpKernel import * 
from LinearErrorKernel import * 

ROOT.gROOT.LoadMacro("inc/HggTwoSidedCBPdf.cxx+") 


########### User Defined Options ##########

# These are all pulled from the input file below: 

from inputs_OptimizeHyperPars import * 

###########################################

### Basic Exponential for GPR Prior 

def mySimpleExponential(x, a, b):
	return a*np.exp( b * x ) 		
	
###########################################	

### A quick function to create histograms from numpy arrays 

def makeHistoFromArray( inputArray , outputHisto , inputError = np.array('i') ): 
	nBins = outputHisto.GetNbinsX() 
	if( len(inputArray) != nBins ): 
		print("WARNING in makeHistoFromArray")
		print("Number of bins in histogram does not match length of input array!")
		print("Returning empty histogram.") 
		return 
	for iBin in range(nBins): 
		outputHisto.SetBinContent( iBin+1 , inputArray[iBin] ) 
		if( inputError.shape ): outputHisto.SetBinError( iBin+1 , inputError[iBin] ) 

###########################################

### A quick function to create numpy arrays from histograms

def makeArrayFromHisto( inputHisto ): 
	nBins = inputHisto.GetNbinsX()
	outputArray = np.zeros( nBins , 'd' )
	for iBin in range(nBins): 
		outputArray[iBin] = inputHisto.GetBinContent( iBin+1 ) 
	return outputArray

###########################################
	
### Generate the signal shape workspace ###

def generateSignalWS( wsName , range_Myy ):

	ws = ROOT.RooWorkspace(wsName)
	ws.factory('rooMyy[{0}, {1}]'.format(range_Myy[0], range_Myy[1]))
	ws.factory('RooM0[{0}, {1}]'.format(range_Myy[0], range_Myy[1]))
	ws.factory('sigWidth[0.0005,1000.0]')
	ws.obj('RooM0').setVal(signalPos)
	ws.obj('sigWidth').setVal(2.0)
	
	### Generate sig pdf
	sigStr = "RooCBShape:sig(rooMyy,RooM0,sigWidth,5,10)"
	sigPDF = ws.factory(sigStr)
	ws.saveSnapshot("initial",ws.allVars())
	
	return ws 
	
###########################################
	
### Generate the workspace (background) ###

def generateBackgroundWS( wsName , whichFunction , range_Myy ):

	ws = ROOT.RooWorkspace(wsName)
	ws.factory('rooMyy[{0}, {1}]'.format(range_Myy[0], range_Myy[1]))
	
	### Generate bkg pdf
	
	# use exponential as default 
	bkgStr = "RooExponential:bkg(rooMyy, Exponential_b[-0.02, -0.5, 0.0])"
	
	# PowerLaw 
	if( whichFunction == "PowerLaw" ): 
		bkgStr = "EXPR:bkg('rooMyy**PowerLaw_b',{rooMyy,PowerLaw_b[-2.0,-20,20]})"
	# Exponential 
	elif( whichFunction == "Exponential" ): 	
		bkgStr = "RooExponential:bkg(rooMyy, Exponential_b[-0.02, -0.5, 0.0])"
	# ExpPoly2 
	elif( whichFunction == "ExpPoly2" ): 	
		bkgStr = "EXPR:bkg('exp((rooMyy-100)/100*(ExpPoly2_b+ExpPoly2_c*(rooMyy-100)/100))',{rooMyy,ExpPoly2_b[-2.0,-10,10],ExpPoly2_c[1.0,-10,10]})"
	# ExpPoly3 
	elif( whichFunction == "ExpPoly3" ):
		bkgStr = "EXPR:bkg('exp((rooMyy-100)/100*(ExpPoly3_b+ExpPoly3_c*(rooMyy-100)/100+ExpPoly3_d*(rooMyy-100)*(rooMyy-100)/(100*100)))',{rooMyy,ExpPoly3_b[-3.7,-10,10],ExpPoly3_c[2.0,-10,10],ExpPoly3_d[0.5,-10,10]})"
	# Bern 3 
	elif( whichFunction == "Bern3" ): 
		bkgStr = "RooBernstein:bkg(rooMyy, {Bern3_b[5,-10,10], Bern3_c[2,-10,10], Bern3_d[2,-10,10], 1})"
	# Bern 4 
	elif( whichFunction == "Bern4" ): 
		bkgStr = "RooBernstein:bkg(rooMyy, {Bern4_b[9,-15,15], Bern4_c[4,-10,10], Bern4_d[3,-10,10], Bern4_e[2,-10,10], 1})"
	# Bern 4 
	elif( whichFunction == "Bern5" ): 
		bkgStr = "RooBernstein:bkg(rooMyy, {Bern5_b[10,-15,15], Bern5_c[7.5,-10,10], Bern5_d[5.5,-10,10], Bern5_e[3,-10,10], Bern5_f[2,-10,10], 1})"
	else: 
		print( "Error - Improper input function specified" ) 
		print( "   Options = PowerLaw , Exponential , ExpPoly2 , ExpPoly3 , Bern3 , Bern4 , Bern5" ) 
		
	bkgPDF = ws.factory(bkgStr)		
	ws.saveSnapshot("initial",ws.allVars())
	
	return ws 

###########################################

### Create the GPR-Smoothed Template ### 

def makeGPRtemplate( nBins , range_Myy , toyTemplate , length_scale , length_scale_slope ): 
	
	x_Myy = np.array( [ range_Myy[0]+1.0*i*(range_Myy[1]-range_Myy[0])/nBins for i in range(nBins) ] )

	### Make array from the toy template 
	
	myyHistArray = makeArrayFromHisto( toyTemplate ) 
	myyErrs = []
	for ibin in range(nBins): 
		myyErrs.append( toyTemplate.GetBinError( ibin+1 ) ) 
	myyErrs = np.array( myyErrs ) 
	
	# Assume Exponential Prior (Recommended) 

	optimal_pars, par_cov = curve_fit(mySimpleExponential, x_Myy, myyHistArray, p0=(myyHistArray[0],-0.02))
	myBaseLine = np.array(mySimpleExponential(x_Myy,optimal_pars[0],optimal_pars[1]))

	### Get estimates of different hyper-parameter bounds from the input template 

	# Set bounds on the scaling of the GPR template (basically a maximum y-range) 

	const_low = 10.0**-5
	const_hi = nEvts * 10.0

	### Define the GPR Kernel 

	kernel = ( ConstantKernel(constant_value=1.0, constant_value_bounds=(const_low,const_hi)) * 
				Gibbs(l0=length_scale,l0_bounds=(length_scale,length_scale),l_slope=length_scale_slope,l_slope_bounds=(length_scale_slope,length_scale_slope)) )

	### Fit the GPR Kernel to the input template 
	
	gpr = GaussianProcessRegressor( kernel=kernel , n_restarts_optimizer = 3 ) 
	gpr.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )
	
	# Check to see if GPR Prediction is effectively the same as the exponential prior 
	# If so, re-do the GPR fitting, but with a flat prior instead of exponential prior 
	gprPrediction, gprCov = gpr.predict( x_Myy.reshape(len(x_Myy),1) , return_cov=True )
	
	if( np.sqrt( (gprPrediction*gprPrediction).sum() )/ myyHistArray.sum() < 0.0001 ): 
		myBaseLine = np.zeros( len(myyHistArray) ) 
		gpr = GaussianProcessRegressor( kernel=kernel , n_restarts_optimizer = 3 , normalize_y=True ) 
		gpr.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )	
		gprPrediction, gprCov = gpr.predict( x_Myy.reshape(len(x_Myy),1) , return_cov=True )

	# Use the GPR template's errors 
	gprError = np.diagonal(gprCov)
	gprPrediction = gprPrediction + myBaseLine
	gprPredictionHisto = ROOT.TH1F( "gprPredictionHisto" , "gprPredictionHisto" , nBins , range_Myy[0] , range_Myy[1] ) 
	makeHistoFromArray( gprPrediction , gprPredictionHisto , gprError )
	
	### Obtain the GPR prediction and convert to a histogram 
		
	# Use the GPR template's errors 
	gprPrediction, gprCov = gpr.predict( x_Myy.reshape(len(x_Myy),1) , return_cov=True )
	gprError = np.diagonal(gprCov)
	gprPrediction = gprPrediction + myBaseLine
	gprPredictionHisto = ROOT.TH1F( "gprPredictionHisto" , "gprPredictionHisto" , nBins , xMin , xMax ) 
	makeHistoFromArray( gprPrediction , gprPredictionHisto , gprError )

	return( gprPrediction , gprError )

###########################################

### Perform the Scan of GPR Hyper Parameters for a Signal Width ### 

def performScan( h_basisBkgPDF , basisSigWS , sigWidth ):

	gprPredictions = []
	gprErrors = []
	gprPredHistos = [] 
	residuals = []
	errVals = [] 

	basisSigWS.obj('sigWidth').setVal( sigWidth )
	h_basisSigPDF = basisSigWS.obj("sig").createHistogram("h_basisSigPDF",basisSigWS.obj("rooMyy"),ROOT.RooFit.Binning(nBins,range_Myy[1],range_Myy[1]))
	h_basisSigPDF.Scale( sigFrac*inHisto.GetSumOfWeights() / h_basisSigPDF.GetSumOfWeights() )
	
	xSig = basisSigWS.obj("RooM0").getValV()
	xLow = xSig - sigWidth
	xHi = xSig + sigWidth
	xUnderSig = np.where((x_Myy>=xLow)&(x_Myy<xHi))
	
	myToy = ROOT.TH1D("myToy","myToy",nBins,range_Myy[0],range_Myy[1])
	myToy.Sumw2()
	myToy.Add(h_basisBkgPDF)
	myToy.Add(h_basisSigPDF)
	for bin in range(myToy.GetNbinsX()): myToy.SetBinError(bin+1,0.0)

	sigAndBkgShape = makeArrayFromHisto( myToy )
	sigOnlyArray = makeArrayFromHisto( h_basisSigPDF )
	bkgOnlyArray = makeArrayFromHisto( h_basisBkgPDF )
	for ls in length_scales:
		gprPredictions.append( [] )
		gprErrors.append( [] )
		gprPredHistos.append( [] )
		residuals.append( [] )
		errVals.append( [] )
		for ls_b in length_scale_slopes: 
			gprPred , gprError = makeGPRtemplate( nBins , range_Myy , myToy , ls , ls_b ) 
			gprPredictions[-1].append( gprPred )
			gprErrors[-1].append( gprError )
			residuals[-1].append( sigAndBkgShape-gprPred )
			errVals[-1].append( np.absolute( sigAndBkgShape[xUnderSig]-gprPred[xUnderSig] ).sum() / sigOnlyArray[xUnderSig].sum() )
			
			hName = "h_gprPred_ls_"+str(ls)+"_lsb_"+str(ls_b)+"_sw_"+"{:4.2f}".format(sigWidth) 
			gprPredHistos[-1].append( ROOT.TH1D(hName,hName,nBins,range_Myy[0],range_Myy[1]) )
			makeHistoFromArray( gprPred , gprPredHistos[-1][-1] , gprError )
			
	### Plot 
	fitCan = ROOT.TCanvas("fitCan","fitCan",800,800)
	upperPad = ROOT.TPad("upperPad", "upperPad", 0.0, 0.3, 1.0, 1.0)
	lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0.0, 0.0, 1.0, 0.3)
	upperPad.Draw() 
	upperPad.SetBottomMargin(0.02)
	lowerPad.Draw() 
	lowerPad.SetTopMargin(0.02) ;
	lowerPad.SetBottomMargin(0.3) ;
	fitLeg = ROOT.TLegend(0.65,0.30,0.93,0.93)
	fitLeg.SetFillColor(-1)
	fitLeg.SetBorderSize(0)
	fitLeg.SetNColumns(3);
	ROOT.gStyle.SetPalette(60)
	
	upperPad.cd() 
	
	myToy.GetYaxis().SetTitle("Events / Bin")
	myToy.SetTitle(" ")
	myToy.SetMarkerSize(1)
	myToy.SetMarkerColor(1)
	myToy.SetLineWidth(0)
	myToy.SetStats(0)
	myToy.GetXaxis().SetTitleSize(0)
	myToy.GetXaxis().SetLabelSize(0)
	myToy.GetYaxis().SetTitle("Events / Bin")
	myToy.SetTitle(" ") 
	myToy.Draw("ep")
	
	for i in range(len(length_scales)):
		for j in range(len(length_scale_slopes)): 
			gprPredHistos[i][j].SetMarkerSize(0)
			gprPredHistos[i][j].SetLineWidth(2)
			gprPredHistos[i][j].Draw("ehist same plc")
			fitLeg.AddEntry( gprPredHistos[i][j] , "#lambda="+str(length_scales[i])+", b_{#lambda}="+str(length_scale_slopes[j]) )
	
	ROOT.ATLAS_LABEL(0.25,0.85)
	ROOT.myText(0.35,0.85,1,"Internal")
	ROOT.myText(0.25,0.81,1,"GPR Fits",0.04)
	ROOT.myText(0.25,0.77,1,catName,0.04)
	ROOT.myText(0.25,0.73,1,"Injected #sigma = "+"{:4.2f}".format(sigWidth),0.04)
	fitLeg.Draw()
	
	lowerPad.cd() 
	refLine = ROOT.TH1I("refLine","refLine",1,range_Myy[0],range_Myy[1])
	refLine.SetBinContent(1,0) 
	refLine.GetXaxis().SetTitle("M_{#gamma#gamma} [GeV]")
	refLine.GetXaxis().SetTitleSize(0.13)
	refLine.GetXaxis().SetTitleOffset(1.0)
	refLine.GetXaxis().SetLabelSize(0.12)
	refLine.GetYaxis().SetTitle("Fit - True")
	refLine.GetYaxis().SetTitleSize(0.125)
	refLine.GetYaxis().SetTitleOffset(0.55)
	refLine.GetYaxis().SetLabelSize(0.12)
	refLine.SetLineColor(1)
	refLine.SetLineWidth(1)
	refLine.SetLineStyle(2)
	refLine.SetMinimum(-0.24)
	refLine.SetMaximum(0.24)
	refLine.Draw()
	
	diffHistos = [] 
	for i in range(len(length_scales)):
		for j in range(len(length_scale_slopes)): 
			hName = "diff_"+gprPredHistos[i][j].GetName()
			diffHistos.append( ROOT.TH1D(hName,hName,nBins,range_Myy[0],range_Myy[1]) )
			diffHistos[-1].Add(myToy,gprPredHistos[i][j],1.0,-1.0)
			diffHistos[-1].Divide(gprPredHistos[i][j])
			diffHistos[-1].SetMarkerSize(0)
			diffHistos[-1].SetLineWidth(2)
			diffHistos[-1].Draw("ehist same plc")		
	
	fitCan.Print("GPRHyperParOpt_Fits_"+inHistName+"_sigma_"+"{:4.2f}".format(sigWidth)+".eps")
	
	return np.array(errVals) 


###########################################
########### The Important Stuff ###########

nCategories = len(templateHistoNames) 
		
if ( len(categoryNames) != nCategories ): 
	print( 	"WARNING - The number of category names passed is not the same as the number of background templates passed! Will ignore the given category names." )
	categoryNames = templateHistoNames 

if ( not not inSignalFileName ): 
	inSignalFile = ROOT.TFile.Open(inSignalFileName) 
	signalFitWS = inSignalFile.Get(inSignalWorkspaceName)
	if( len(signalFileSigmaNames) != nCategories ): 
		print( "WARNING - The number of signal parameter names passed is not the same as the number of background templates passed! Program will crash." )
	signalWidths = [] 
	for iCat in range(nCategories): 
		signalWidths.append( (signalFitWS.obj(signalFileSigmaNames[iCat])).getVal() )

if ( not inSignalFileName ):
	if ( len(signalWidths) == 1 ): signalWidths = [ signalWidths[0] for i in range(nCategories) ] 
	if ( len(signalWidths) != nCategories ): 
		print( 	"WARNING - The number of signal widths passed is not the same as the number of background templates passed! Will default to using the first provided signal width." )
		signalWidths = [ signalWidths[0] for i in range(nCategories) ] 

outputTrees = [] 
outFile = ROOT.TFile(outFileName,"RECREATE") 
inFile = ROOT.TFile.Open(inFileName,"READ")
ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasStyle.C") 
ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasUtils.C") 
ROOT.SetAtlasStyle()

b_length_scale_min = []
b_length_scale_max = []
b_length_scale_slope_min = []
b_length_scale_slope_max = []

for iCat in range(nCategories): 

	### Set the input parameters 
	
	inFile.cd() 
	inHistName = templateHistoNames[ iCat ] 
	catName = categoryNames[ iCat ] 
	expectedSigWidth = signalWidths[ iCat ] 

	### Import the input histogram

	inHisto = inFile.Get(inHistName)
	inHisto.Rebin( nRebin )

	nBins = inHisto.GetNbinsX()
	xMin = inHisto.GetBinLowEdge(1)
	xMax = inHisto.GetBinLowEdge(nBins+1)
	range_Myy = [ xMin , xMax ]
	x_Myy = np.array( [ xMin+1.0*i*(xMax-xMin)/nBins for i in range(nBins) ] )
	binWidth = 1.0*(xMax-xMin)/nBins

	mcErrs = [] 
	for i in range(nBins): 
		mcErrs.append( inHisto.GetBinError(i+1) )
	mcErrs = np.array(mcErrs)	

	# Get some information from the input template  

	nEvts = int(inHisto.GetEntries())
	nBins = inHisto.GetNbinsX()
	xMin = inHisto.GetBinLowEdge(1)
	xMax = inHisto.GetBinLowEdge(nBins+1)
	x_Myy = np.array( [ xMin+1.0*i*(xMax-xMin)/nBins for i in range(nBins) ] )

	### Determine the Optimal Basis Analytical Function to Use 

	allFunctions = [ "PowerLaw" , "Exponential" , "ExpPoly2" , "ExpPoly3" , "Bern3" , "Bern4" , "Bern5" ]
	nDOF = []
	allChiSqrd = []
	for i,testFunction in enumerate(allFunctions): 
		testBkgWS = generateBackgroundWS( "test"+testFunction+"WS" , testFunction , range_Myy ) 
		testerRooHist = ROOT.RooDataHist("testerRooHist", "testerRooHist", ROOT.RooArgList(testBkgWS.obj("rooMyy")), inHisto)
		testBkgWS.pdf("bkg").fitTo( testerRooHist , ROOT.RooFit.SumW2Error(ROOT.kTRUE)) 
		testFrame = testBkgWS.obj('rooMyy').frame() 
		testerRooHist.plotOn( testFrame , ROOT.RooFit.MarkerColor(1) )
		testBkgWS.pdf('bkg').plotOn( testFrame , ROOT.RooFit.LineColor(415) , ROOT.RooFit.LineWidth(6) , ROOT.RooFit.Name('bkgOnlyFit')) 		
		nDOF.append( testBkgWS.pdf('bkg').getVariables().getSize() )
		allChiSqrd.append( testFrame.chiSquare(nDOF[i]) )
	
	nDOF = np.array(nDOF,'i')	
	allChiSqrd = np.array(allChiSqrd)
	#bestFunctionNum = np.where( allChiSqrd == allChiSqrd[allChiSqrd>=1.0].min() )[0][0]
	bestFunctionNum = np.where( allChiSqrd == allChiSqrd.min() )[0][0]
	whichFunction = allFunctions[bestFunctionNum]

	### Define a histogram with the signal and background shapes 

	basisSigWS = generateSignalWS( "signalWS" , range_Myy )
	basisBkgWS = generateBackgroundWS( "backgroundWS" , whichFunction , range_Myy )
	templateRooHist = ROOT.RooDataHist("templateRooHist", "templateRooHist", ROOT.RooArgList(basisBkgWS.obj("rooMyy")), inHisto) 

	### Fit the given histogram with the specified background shape 

	basisBkgWS.obj("bkg").fitTo( templateRooHist )
	h_basisBkgPDF = basisBkgWS.obj("bkg").createHistogram("h_basisBkgPDF",basisSigWS.obj("rooMyy"),ROOT.RooFit.Binning(nBins,range_Myy[1],range_Myy[1])) #ROOT.RooFit.Scaling(ROOT.kFALSE),ROOT.RooFit.Scaling(ROOT.kFALSE),ROOT.RooFit.Extended(ROOT.kFALSE))
	h_basisBkgPDF.Scale( inHisto.GetSumOfWeights() / h_basisBkgPDF.GetSumOfWeights() )

	### Now Run! 

	errVals = []
	for sigWidth in [ 0.5*binWidth , expectedSigWidth ]:
		errVals.append( performScan( h_basisBkgPDF , basisSigWS , sigWidth ) )

	### Plot the best hyperparameters 
	
	xBins = np.array(length_scales,'d')
	yBins = np.array(length_scale_slopes,'d')
	
	hyperParCan = ROOT.TCanvas("hyperParCan","hyperParCan",800,600)
	outHistoName = "goodHyperPars_"+inHistName
	goodValHisto = ROOT.TH2I(outHistoName,outHistoName,len(xBins)-1,xBins,len(yBins)-1,yBins)
	goodValArray = np.array((len(xBins)-1,len(yBins)-1),dtype='i')
	
	# Determine the best minimum length scale (GPR fit smoothes out at least half of narrow peak) 
	for i in range(len(length_scales)-1): 
		for j in range(len(length_scale_slopes)-1): 
			if ( errVals[0][i,j] >= 0.33 ): goodValHisto.Fill( length_scales[i] , length_scale_slopes[j] , 1 ) 

	# Determine the best maximum length scale (GPR fit retains at least 80% of wider peak) 
	for i in range(len(length_scales)-1): 
		for j in range(len(length_scale_slopes)-1): 
			if ( errVals[1][i,j] <= 0.25 and 
				errVals[1][i+1,j] <= 0.25 and 
				errVals[1][i,j+1] <= 0.25 ): goodValHisto.Fill( length_scales[i] , length_scale_slopes[j] , 1 ) 
	
	# Fill the output histogram with a value of 1 if the hyper params pass both conditions above 
	for xbin in range(goodValHisto.GetNbinsX()): 
		for ybin in range(goodValHisto.GetNbinsY()): 
			bin = goodValHisto.GetBin(xbin+1,ybin+1)
			if( goodValHisto.GetBinContent(bin) == 2 ): goodValHisto.SetBinContent(bin,1)
			else: goodValHisto.SetBinContent(bin,0)	
	
	# Select a rectangular area out of the optimal hyper params such that the length scale bounds are the widest possible, and favoring larger length scales 	
	holder_ls_bounds = [] 
	for ybin in range(goodValHisto.GetNbinsY()): 
		ls_min = 999
		ls_max = -999 
		for xbin in range(goodValHisto.GetNbinsX()): 
			bin = goodValHisto.GetBin(xbin+1,ybin+1)
			if( goodValHisto.GetBinContent(bin) == 1 ): 
				if( ls_min > goodValHisto.GetXaxis().GetBinLowEdge(xbin+1) ): ls_min =goodValHisto.GetXaxis().GetBinLowEdge(xbin+1)
				if( ls_max < goodValHisto.GetXaxis().GetBinLowEdge(xbin+2) ): ls_max =goodValHisto.GetXaxis().GetBinLowEdge(xbin+2)
		holder_ls_bounds.append([ls_min,ls_max])
	
	bls_bounds = [-1,-1] 
	ls_bounds = [-1,-1]
	lswidth = 0 
	for ybin in range(goodValHisto.GetNbinsY()): 
		if( lswidth < holder_ls_bounds[ybin][1]-holder_ls_bounds[ybin][0] ): 
			lswidth = holder_ls_bounds[ybin][1]-holder_ls_bounds[ybin][0] 
			bls_bounds = [ yBins[ybin] , yBins[ybin+1] ]
			ls_bounds = holder_ls_bounds[ybin]
		elif( lswidth == holder_ls_bounds[ybin][1]-holder_ls_bounds[ybin][0] and holder_ls_bounds[ybin]==holder_ls_bounds[ybin-1] ): 
			bls_bounds[1] = yBins[ybin+1] 
			ls_bounds = holder_ls_bounds[ybin]
		elif( lswidth == holder_ls_bounds[ybin][1]-holder_ls_bounds[ybin][0] and holder_ls_bounds[ybin][1]>holder_ls_bounds[ybin-1][1] ): 
			bls_bounds = [ yBins[ybin] , yBins[ybin+1] ]
			ls_bounds = holder_ls_bounds[ybin]

	# Fill the output histogram with a value of 2 if the hyper params are in the optimal rectangle 
	for xbin in range(goodValHisto.GetNbinsX()): 
		for ybin in range(goodValHisto.GetNbinsY()): 
			bin = goodValHisto.GetBin(xbin+1,ybin+1)
			if( goodValHisto.GetXaxis().GetBinCenter(xbin+1) > ls_bounds[0] and 
				goodValHisto.GetXaxis().GetBinCenter(xbin+1) < ls_bounds[1] and 
				goodValHisto.GetYaxis().GetBinCenter(ybin+1) > bls_bounds[0] and 
				goodValHisto.GetYaxis().GetBinCenter(ybin+1) < bls_bounds[1] ):
				goodValHisto.SetBinContent(bin,2)
	
	# Stylize the output plot 
	goodValHisto.SetStats(0)
	goodValHisto.GetXaxis().SetTitle("Gibbs Length Scale [GeV]")
	goodValHisto.GetYaxis().SetTitle("Gibbs Length Scale Slope")
	goodValHisto.SetTitle(" ") 
	ROOT.gStyle.SetPalette(70)
	ROOT.TColor.InvertPalette()
	goodValHisto.Draw("col")
	ROOT.ATLAS_LABEL(0.20,0.85)
	ROOT.myText(0.32,0.85,1,"Internal")
	ROOT.myText(0.20,0.81,1,"GPR Hyper Parameters",0.04)
	ROOT.myText(0.20,0.77,1,catName,0.04)
	hyperParCan.Update() 
	hyperParCan.Print("GoodHyperPars_"+inHistName+".eps")
		
	goodValHisto.SetName(outHistoName) 
	outFile.cd() 
	goodValHisto.Write()
	
	# Make a ROOT tree with the selected hyper-parameter rectangular area 
	outputTrees.append( ROOT.TTree("HyperParArea_"+inHistName,"HyperParArea_"+inHistName) )
	
	b_length_scale_min.append( np.zeros(1, dtype=float) )
	b_length_scale_max.append( np.zeros(1, dtype=float) )
	b_length_scale_slope_min.append( np.zeros(1, dtype=float) )
	b_length_scale_slope_max.append( np.zeros(1, dtype=float) )
	
	outputTrees[-1].Branch("length_scale_min",b_length_scale_min[-1],"length_scale_min/D")
	outputTrees[-1].Branch("length_scale_max",b_length_scale_max[-1],"length_scale_max/D")
	outputTrees[-1].Branch("length_scale_slope_min",b_length_scale_slope_min[-1],"length_scale_slope_min/D")
	outputTrees[-1].Branch("length_scale_slope_max",b_length_scale_slope_max[-1],"length_scale_slope_max/D")
	
	b_length_scale_min[-1][0] = ls_bounds[0] 
	b_length_scale_max[-1][0] = ls_bounds[1] 
	b_length_scale_slope_min[-1][0] = bls_bounds[0] 
	b_length_scale_slope_max[-1][0] = bls_bounds[1] 
	
	outputTrees[-1].Fill() 
	
outFile.cd() 
for tree in outputTrees:
	tree.Write() 

print( "All done!" )	